import React from "react";
import welcomeLogin from "./assets/undraw_welcome_cats_thqn.png";
import Signup from "./components/Signup";

import "./App.css";

const App = () => {
  return (
    <div>
      <div className="container mt-3">
        <div className="row">
          <div className="col-md-5">
            <Signup />
          </div>
          <div className="col-md-7">
            <img className="img-fluid w-100" src={welcomeLogin} alt="pic" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
